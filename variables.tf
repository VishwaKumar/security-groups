variable "vnf_vpc_name" {
  description = "vnf_vpc name"
  type        = string
}

variable "vnf_sg_name" {
  description = "vnf-sg name"
  type        = string
}
